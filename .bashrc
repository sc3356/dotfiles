# .bashrc

# User specific aliases and functions

# Add whatever you want to see after logging in here.

# Specify that ^D won't log you out.
set -o ignoreeof 

# make vi the command-line editor
set -o vi

# keep > from clobbering existing files
set -C	# set -o noclobber
set bg=dark
# can't bug me w/talk or write
#mesg n

alias l="ls -CF"
if [[ `uname -s` = "Linux" ]] ; then
	alias ls="ls --color=auto"
fi
alias r="fc -s"
alias h="history"
alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
alias dirs='dirs -v'
alias l='ls'
alias ls='ls -l'
alias Vim='vim -R'  # read-only

#made it into a script.  Should be a function here.
#alias lh='ls -ot | head'
function lh
{
	ls -ot "${1:-.}" | head
}

alias vii='vi -c "set bg=light"'

#unalias s
alias u="cd .."

# set prompt, though maybe already done in /etc/bashrc
#		see man or info pages for special characters (\u, etc.)
#export PS1="\u@\h \W> "
export PS1="\[\033[01;32m\]\u@\h\[\033[01;31m\] \w\n$\[\033[00m\] "

PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%.*}: ${PWD##*/}\007"'

export JAVA_HOME=`/usr/libexec/java_home -v 1.7.0_75`

export ANDROID_HOME=~/Library/Android/sdk
export ANDROID_BIN=$ANDROID_HOME/platform-tools/
export ANDROID_TOOLS=$ANDROID_HOME/tools
export ECLIPSE_HOME=/Applications/Eclipse.app/Contents/Eclipse
export EMU="-r/Users/stephencantwell/inferno-os -c1 -g1920x1008"
export INFERNO_HOME=/Users/stephencantwell/inferno-os
export INFERNO_BIN=$INFERNO_HOME/MacOSX/386/bin
export MACPORTS_BIN=/opt/local/bin
export MACPORTS_SBIN=/opt/local/sbin
export MACPORTS_MANPATH=/opt/local/share/man
export MYSQL_BIN=/opt/local/lib/mysql56/bin:$PATH
export PYTHON27_BIN=/opt//local/Library/Frameworks/Python.framework/Versions/2.7/bin/:$PATH

export
PATH=$MACPORTS_BIN:$MACPORTS_SBIN:$ANDROID_HOME:$ANDROID_BIN:$ANDROID_TOOLS:$ECLIPSE_HOME:$INFERNO_HOME:$INFERNO_BIN:$MYSQL_BIN:$PYTHON27_BIN
export MANPATH=$MACPORTS_MANPATH:$MANPATH
