:filetype on

set nocompatible
set tabstop=3
set shiftwidth=3
set expandtab
set showmode
set showmatch
set textwidth=76
set pastetoggle=<f2>
set backspace=2
set hlsearch
set ruler
set bg=dark
set clipboard=unnamed
if &t_Co > 1
	syntax enable
endif

