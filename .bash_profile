#
# ~/.bash_profile
#
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
export MANPATH=/opt/local/share/man:$MANPATH

export INFERNO_HOME=/Users/stephencantwell/inferno-os
export PATH=$INFERNO_HOME/MacOSX/386/bin:/Users/stephencantwell/Library/Android/tools/:$PATH
export EMU="-r/Users/stephencantwell/inferno-os -c1 -g1920x1008"


export PATH=/opt/local/lib/mysql56/bin:$PATH
[[ -f ~/.bashrc ]] && . ~/.bashrc
